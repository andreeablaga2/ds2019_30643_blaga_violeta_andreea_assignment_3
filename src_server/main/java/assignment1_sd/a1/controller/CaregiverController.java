package assignment1_sd.a1.controller;

import assignment1_sd.a1.entities.Caregiver;
import assignment1_sd.a1.entities.Patient;
import assignment1_sd.a1.services.CaregiverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/caregivers")
public class CaregiverController {
    @Autowired
    private CaregiverService caregiverService;

    @PostMapping()
    public Integer insertCaregiver(@RequestBody Caregiver caregiver){
        return caregiverService.create(caregiver);
    }

    @GetMapping("/all")
    public List<Caregiver> retrieveCaregivers(){
        return caregiverService.findAll();
    }

    @GetMapping("/{id}")
    public Caregiver retrieveCaregiverByID(@PathVariable int id) {
        return caregiverService.findCaregiverByID(id);
    }

    @PutMapping("/{id}")
    public Caregiver updateCaregiver(@RequestBody Caregiver caregiver, @PathVariable int id) {
        return caregiverService.update(caregiver,id);
    }

    @DeleteMapping("/{id}")
    public List<Caregiver> deleteCaregiver(@PathVariable int id){
        return caregiverService.delete(id);
    }

    @GetMapping("/{id}/patients")
    public List<Patient> getPatients(@PathVariable int id) {
        return caregiverService.viewPatients(id);
    }
}
