package assignment1_sd.a1.util;

import assignment1_sd.a1.entities.Medication;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JTableGenerator {
    //transformam din obiect in vector de string-uri
    public static String[] retrieveProperties(Object object)
    {
        String[] properties = new String[6];
        int i=0;
        for (Field field : object.getClass().getDeclaredFields()) {
            field.setAccessible(true); // set modifier to public
            Object value;
            try {
                value = field.get(object);
                //System.out.println(field.getName() + "=" + value);
                properties[i]=value.toString();
                i++;

            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }

        }
        return properties;
    }

    //luam medicamentul din rezultat, il adaugam la o lista si returnam lista de medicamente
    public static List<Medication> createObjectsM(ResultSet resultSet)
    {
        List<Medication> list = new ArrayList<Medication>();

        try {
            while (resultSet.next()) {
                int idM= resultSet.getInt("medicationID");
                String numeM= resultSet.getString("name");

                Medication m = new Medication(idM, numeM);
                System.out.println(m);
                list.add(m);
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    //luam listele si le punem in matrice de string-uri
    public static Object[][] medData(List<Medication> medicamente)
    {
        Object[][] dateMed = new Object[15][6];
        String[] med = new String[6];
        int i=0;
        for(Medication m: medicamente)
        {
            med=retrieveProperties(m);
            dateMed[i]=med;
            i++;
        }
        return dateMed;
    }
}
