package view;

import assignment1_sd.a1.grpc.Med;
import assignment1_sd.a1.grpc.Med.MedPlan;
import dispenser.Dispenser;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

public class View extends JFrame {

    JTable tableM=new JTable();

    JFrame frame = new JFrame("Pill Dispenser");

    private JPanel panel = new JPanel();
    private JPanel timepanel = new JPanel();
    private JPanel medpanel = new JPanel();

    private JLabel currTime = new JLabel("Current time:");
    private JLabel timerlabel = new JLabel();
    private JLabel datelabel = new JLabel();

    Timer timer;
    int hour;

    private static Object[][] dateMed = new Object[25][5];
    private String[] med = {"Patient", "Medication", "Start time", "End time"};
    private static JTable tabel;

    

    Dispenser dispenser = new Dispenser();
    List<MedPlan> meds = dispenser.download();

    private JButton medTakenB = new JButton("Taken");

    public View() {
        frame.add(panel);
        //panel.setBounds(0,0,500,50);

        panel.add(timepanel);
        timepanel.add(currTime);
        timepanel.add(timerlabel);
        timepanel.add(datelabel);


        ActionListener actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Date date = new Date();
                DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
                String time = timeFormat.format(date);
                timerlabel.setText(time);
                hour = date.getHours();
                //System.out.println(date.getHours()-date.getMinutes());

                Date date2 = new Date();
                DateFormat timeFormat2 = new SimpleDateFormat("dd/MM/yyyy");
                String time2 = timeFormat2.format(date2);
                datelabel.setText(time2);

            }

        };
        timer = new Timer(500,actionListener);
        timer.setInitialDelay(0);
        timer.start();

        panel.add(medpanel);
        tabel = new JTable(medplanData,columns);
        JScrollPane scrollPane = new JScrollPane(tabel);
        panel.add(scrollPane);
        medpanel.add(medTakenB);
        medTakenB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedrow = -1;
                selectedrow = tabel.getSelectedRow();
                dispenser.download();
            }
        });

        //panel.add(medpanel);
        // medpanel.add(medTakenB);
        //medTakenB.addActionListener(new Action());
       // medpanel.setLayout(new GridBagLayout());
//        medpanel.setLayout(new BoxLayout(medpanel, BoxLayout.Y_AXIS));
      //  medTakenB.setBounds(260,400,100,20);

        meds.forEach( medication -> {
            JButton medTakenB = new JButton();
            medTakenB.setText(medication.getName());
            //medTakenB.setSize(110, 110);

            buttonMap.put(medication, medTakenB);

            medpanel.add(medTakenB);

        });

        //frame.pack();
        //frame.setLayout(null);
        frame.setSize(500,500);
        frame.setVisible(true);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        

    }

//    public JButton getMedTakenB() {
//        medTakenB.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                JOptionPane.showMessageDialog(medTakenB, "Medication Taken!");
//            }
//        });
//        return medTakenB;
//    }

//    class Action implements ActionListener{
//        public void actionPerformed (ActionEvent e){
//            JOptionPane.showMessageDialog(medTakenB, "Medication Taken!");
//        }
//    }



//    //punem datele in tabel
//    void setTableM(Object[][] rows, String[]  columns)
//    {
//        TableModel model=new DefaultTableModel(rows,columns);
//        tableM.setModel(model);
//    }
//
//    JTable getTableM()
//    {
//        return tableM;
//    }
//
//    void addMedTakenBListener(ActionListener m){
//        medTakenB.addActionListener(m);
//    }

}
