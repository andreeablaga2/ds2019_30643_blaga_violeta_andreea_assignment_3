package dispenser;

import assignment1_sd.a1.grpc.Med;
import assignment1_sd.a1.grpc.dispenserGrpc;
import assignment1_sd.a1.grpc.Med.MedPlan;
import assignment1_sd.a1.grpc.Med.PatientRequest;
import assignment1_sd.a1.grpc.Med.DispenserAPIResponse;
import assignment1_sd.a1.grpc.Med.Request;
import assignment1_sd.a1.grpc.Med.APIResponse;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import java.util.List;

public class Dispenser {
    ManagedChannel channel;

    dispenserGrpc.dispenserBlockingStub dispenserStub;

    public List<MedPlan> download(){
        System.out.println("is in download la client");
        channel = ManagedChannelBuilder.forAddress("localhost",6565).usePlaintext().build();
        dispenserStub = dispenserGrpc.newBlockingStub(channel);

        PatientRequest patientRequest = PatientRequest.newBuilder().setId(1).build();
        DispenserAPIResponse dispenserAPIresponse = dispenserStub.download(patientRequest);

        channel.shutdown();

        return dispenserAPIresponse.getDispenserAPIresponseList();
    }

    public String notifyy(){
        System.out.println("is in notify la client");
        channel = ManagedChannelBuilder.forAddress("localhost",6565).usePlaintext().build();
        dispenserStub = dispenserGrpc.newBlockingStub(channel);

        Request notifyrequest = Request.newBuilder().setMessage("1").build();
        APIResponse response = dispenserStub.notify(notifyrequest);

        channel.shutdown();

        return response.getResponsemessage();
    }

//    public String medNotTaken(){
//        System.out.println("not taken in client");
//        channel = ManagedChannelBuilder.forAddress("localhost",6565).usePlaintext().build();
//        dispenserStub = dispenserGrpc.newBlockingStub(channel);
//    }

}
